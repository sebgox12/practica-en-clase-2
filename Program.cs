using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica_en_clase_2
{
    class Program
    {
        public struct clientes
        {
            public int Monto_Cuenta;
            public string nombre;
        }

        static void Main(string[] args)
        {
            /*Crear un programa que simule un banco que tiene 3 clientes que pueden hacer depósitos y retiros. 
             * También el banco requiere que al final del día calcule la cantidad de dinero que hay depositado.*/

            Console.WriteLine("ingresa el nombre de los tres clientes");
            clientes[] customer = new clientes[3];

            int DineroBanco=0;
            customer[0].Monto_Cuenta=0;
            customer[1].Monto_Cuenta=0;
            customer[2].Monto_Cuenta=0;

            for (int i = 0; i < 3; i++)
            {
                customer[i].nombre = Console.ReadLine();

                Console.WriteLine("\n El cliente " + i + " se llama " + customer[i].nombre + "\n");
            }

            int r = 0;

            while (r == 0)
            {
                Console.WriteLine("Pulse 1 para depositar, 2 para retirar, 3 para transferir y cuatro para verificar la cuenta");

                short op = short.Parse(Console.ReadLine());

                switch (op)
                {
                    case 1:

                        Console.WriteLine("Al cliente numero que le quiere depositar");

                        short i = short.Parse(Console.ReadLine());

                        Console.WriteLine("Cuanto desea depositar");

                        int montoDep = int.Parse(Console.ReadLine());

                        customer[i].Monto_Cuenta = customer[i].Monto_Cuenta + montoDep;

                        Console.WriteLine("Deposito {0} pesos en la cuenta de {1}. Ahora tiene {2}"
                            ,montoDep,customer[i].nombre,customer[i].Monto_Cuenta);

                        DineroBanco = DineroBanco + montoDep;

                        break;

                    case 2:

                        Console.WriteLine("Que cliente desea retirar");

                        i = short.Parse(Console.ReadLine());

                        Console.WriteLine("Cuanto desea retirar");

                        int montoRet = int.Parse(Console.ReadLine());

                        if (montoRet <= customer[i].Monto_Cuenta && montoRet>0)
                        {
                            customer[i].Monto_Cuenta = customer[i].Monto_Cuenta - montoRet;

                            Console.WriteLine("Retiro {0} pesos en la cuenta de {1}. Ahora tiene {2}"
                                , montoRet, customer[i].nombre, customer[i].Monto_Cuenta);

                            DineroBanco = DineroBanco - montoRet;
                        }

                        else { Console.WriteLine("No puede retirar esta cantidad"); }

                        break;

                    case 3:

                        Console.WriteLine("Que cliente desea efectuar la transaccion(Ingresar numero del cliente)");

                        i = short.Parse(Console.ReadLine());

                        Console.WriteLine("Hola {0} a quien desea transferir", customer[i].nombre);

                        short ir = short.Parse(Console.ReadLine());

                        Console.WriteLine("Cuanto le desea transferir a " + customer[ir].nombre);

                        int MontoTrans = int.Parse(Console.ReadLine());

                        if (MontoTrans <= customer[i].Monto_Cuenta && MontoTrans > 0)
                        {
                            customer[i].Monto_Cuenta = customer[i].Monto_Cuenta - MontoTrans;

                            customer[ir].Monto_Cuenta = customer[ir].Monto_Cuenta + MontoTrans;
                        }

                        break;

                    case 4:

                        Console.WriteLine("Que cuanta desea ver(ingresar numero de esta)");

                        i = short.Parse(Console.ReadLine());

                        Console.WriteLine("{0} tiene {1} en la cuenta"
                            , customer[i].nombre, customer[i].Monto_Cuenta);
                        break;
                }

                Console.WriteLine("Si quiere hacer otra accion ingrese el 0 de lo contrario ingrese otro numero");

                r = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("\n El banco tiene " + DineroBanco);

            Console.ReadKey();


        }
    }

}
